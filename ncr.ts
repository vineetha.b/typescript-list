import factorial from './factorial';
const ncr = (n: number, r: number): number => {
   return factorial(n) / (factorial(r) * factorial(n - r));
}
export default ncr;